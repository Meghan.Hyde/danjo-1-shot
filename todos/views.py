from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm, TodoUpdateForm


def todo_list_list(request):
    todos = TodoList.objects.all()

    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)

    context = {
        "todo_object": todo,

    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.user = request.user
            todo.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm()

        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoUpdateForm(request.POST, id=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoUpdateForm()

        context = {
            "form": form,
        }
        return render(request, "todos/edit.html", context)
